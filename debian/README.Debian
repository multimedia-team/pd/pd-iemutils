iem_utils for Debian
====================

# Pd-libraries

Debian (like upstream) builds the various libraries as a multi-object library.

In order to use it, you have to load the library first, using one of the
following methods:

- add "LIBRARYNAME" to the list of startup libraries in Pd's preferences
- start Pd with "-lib LIBRARYNAME"

In order to make a patch automatically load a given library whenever the patch
is loaded, add the following object to your patch:
  [declare -stdlib LIBRARYNAME]

NOTE
----
make sure to replace LIBRARYNAME by an actual library name, e.g. one of:
 - iem_adaptfilt
 - iem_roomsim
 - iem_spec2
 - iem_tab

# Pd GUI plugins

The pd-iemutils package includes a few GUI plugins.
The pd-gui automatically loads all GUI plugins it can find in the standard
paths, and there is no way to disable a specific plugin.
Since not all plugins are desirable for all users under all circumstances,
Debian ships the plugins in a separate (unsearched) directory, and leaves the
task of enabling them to the user.

To see a list of plugins run:

    ls /usr/*/pd-gui/plugins-available

To enable a given plugin (e.g. the 'kiosk-plugin') create a symlink into your
~/pd-externals directory

    cd ~/pd-externals
    ln -s /usr/share/pd-gui/plugins-available/kiosk-plugin .

To disable the plugin, simply remove the symlink.

Instead of manually creating the symlinks, you can also use the 'pd-gui-plugin'
cmdline tool (available in the puredata-gui package >= 0.46.7-3)

After you have enabled/disabled a plugin, you need to restart Pd in order to see
an effect.


 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 17 Feb 2016 15:49:39 +0100

