/* For information on usage and redistribution, and for a DISCLAIMER OF ALL
* WARRANTIES, see the file, "LICENSE.txt," in this distribution.

iem_dp written by IOhannes m zmoelnig, Thomas Musil, Copyright (c) IEM KUG Graz Austria 1999 - 2007 */
/* double precision library */

#include "m_pd.h"
#include "iemlib.h"
#include "iem_dp.h"


/* ------------------------  sub__ or -__  ---------------------------- */
/* based on miller's -, which is part of pd, only with double precision */

static t_class *sub___class;

typedef struct _sub__
{
  t_object  x_obj;
  t_float   x_coarse_left;
  t_float   x_fine_left;
  t_float   x_coarse_right;
  t_float   x_fine_right;
  t_outlet  *x_out_coarse;
  t_outlet  *x_out_fine;
} t_sub__;

static void sub___bang(t_sub__ *x)
{
  double ddiff;
  t_float fdiff;

  ddiff = iem_dp_calc_sum(x->x_coarse_left, x->x_fine_left) - iem_dp_calc_sum(x->x_coarse_right, x->x_fine_right);
  fdiff = iem_dp_cast_to_float(ddiff);
  outlet_float(x->x_out_fine, iem_dp_calc_residual(ddiff, fdiff));
  outlet_float(x->x_out_coarse, fdiff);
}

static void sub___float(t_sub__ *x, t_floatarg f)
{
  x->x_coarse_left = f;
  sub___bang(x);
}

static void *sub___new(t_symbol *s, int ac, t_atom *av)
{
  t_sub__ *x = (t_sub__ *)pd_new(sub___class);

  floatinlet_new(&x->x_obj, &x->x_fine_left);
  floatinlet_new(&x->x_obj, &x->x_coarse_right);
  floatinlet_new(&x->x_obj, &x->x_fine_right);
  x->x_coarse_left = 0.0f;
  x->x_fine_left = 0.0f;
  if((ac > 0) && (IS_A_FLOAT(av, 0)))
    x->x_coarse_right = atom_getfloatarg(0, ac, av);
  else
    x->x_coarse_right = 0.0f;
  if((ac > 1) && (IS_A_FLOAT(av, 1)))
    x->x_fine_right = atom_getfloatarg(1, ac, av);
  else
    x->x_fine_right = 0.0f;
  x->x_out_coarse = outlet_new(&x->x_obj, &s_float);
  x->x_out_fine = outlet_new(&x->x_obj, &s_float);
  return (x);
}

static void sub___free(t_sub__ *x)
{
}

void sub___setup(void)
{
  sub___class = class_new(gensym("sub__"), (t_newmethod)sub___new, 0, sizeof(t_sub__), 0, A_GIMME, 0);
  class_addcreator((t_newmethod)sub___new, gensym("-__"), A_GIMME, 0);
  class_addcreator((t_newmethod)sub___new, gensym("-''"), A_GIMME, 0);
  class_addbang(sub___class, sub___bang);
  class_addfloat(sub___class, sub___float);
}
