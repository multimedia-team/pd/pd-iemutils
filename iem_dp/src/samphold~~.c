/* For information on usage and redistribution, and for a DISCLAIMER OF ALL
* WARRANTIES, see the file, "LICENSE.txt," in this distribution.

iem_dp written by IOhannes m zmoelnig, Thomas Musil, Copyright (c) IEM KUG Graz Austria 1999 - 2007 */
/* double precision library */


#include "m_pd.h"
#include "iemlib.h"
#include "iem_dp.h"

/* ---------------- samphold~~ - double precision sample and hold  ----------------- */
/* based on miller's samphold~ which is part of pd */

typedef struct samphold_tilde_tilde
{
    t_object x_obj;
    t_float x_f;
          t_sample  *x_in1_coarse;
    t_sample  *x_in1_fine;
    t_sample  *x_in2_coarse;
    t_sample  *x_in2_fine;
    t_sample  *x_out_coarse;
    t_sample  *x_out_fine;
    double x_lastin;
    double x_lastout;
} t_samphold_tilde_tilde;

t_class *samphold_tilde_tilde_class;

static void *samphold_tilde_tilde_new(void)
{
  t_samphold_tilde_tilde *x = (t_samphold_tilde_tilde *)pd_new(samphold_tilde_tilde_class);

  inlet_new(&x->x_obj, &x->x_obj.ob_pd, &s_signal, &s_signal);
  inlet_new(&x->x_obj, &x->x_obj.ob_pd, &s_signal, &s_signal);
  inlet_new(&x->x_obj, &x->x_obj.ob_pd, &s_signal, &s_signal);
  outlet_new(&x->x_obj, &s_signal);
  outlet_new(&x->x_obj, &s_signal);
  x->x_lastin = 0;
  x->x_lastout = 0;
  x->x_f = 0;
  return (x);
}

static t_int *samphold_tilde_tilde_perform(t_int *w)
{
  t_samphold_tilde_tilde *x = (t_samphold_tilde_tilde *)(w[1]);
  int n = (int)(w[2]), i;
  t_sample *in1c = x->x_in1_coarse;
  t_sample *in1f = x->x_in1_fine;
  t_sample *in2c = x->x_in2_coarse;
  t_sample *in2f = x->x_in2_fine;
  t_sample *outc = x->x_out_coarse;
  t_sample *outf = x->x_out_fine;
  double lastin = x->x_lastin;
  double d_lastout = x->x_lastout;
  t_float f_lastout = 0.0f;

  for(i = 0; i < n; i++, in1c++, in1f++)
  {
    double next = iem_dp_calc_sum(*in2c++, *in2f++);

    if(next < lastin)
          d_lastout = iem_dp_calc_sum(*in1c, *in1f);
        f_lastout = iem_dp_cast_to_float(d_lastout);
    *outf++ = iem_dp_calc_residual(d_lastout, f_lastout);
        *outc++ = f_lastout;
    lastin = next;
  }
  x->x_lastin = lastin;
  x->x_lastout = d_lastout;
  return(w+3);
}

static void samphold_tilde_tilde_dsp(t_samphold_tilde_tilde *x, t_signal **sp)
{
  x->x_in1_coarse = sp[0]->s_vec;
  x->x_in1_fine = sp[1]->s_vec;
  x->x_in2_coarse = sp[2]->s_vec;
  x->x_in2_fine = sp[3]->s_vec;
  x->x_out_coarse = sp[4]->s_vec;
  x->x_out_fine = sp[5]->s_vec;
  dsp_add(samphold_tilde_tilde_perform, 2, x, sp[0]->s_n);
}

static void samphold_tilde_tilde_reset(t_samphold_tilde_tilde *x, t_symbol *s, int argc, t_atom *argv)
{
  t_float coarse, fine;

  if((argc > 0) && (IS_A_FLOAT(argv, 0)))
    coarse = atom_getfloatarg(0, argc, argv);
  else
    coarse = 1.0e20f;
  if((argc > 1) && (IS_A_FLOAT(argv, 1)))
    fine = atom_getfloatarg(1, argc, argv);
  else
    fine = 0.0f;
  x->x_lastin = iem_dp_calc_sum(coarse, fine);
}

static void samphold_tilde_tilde_set(t_samphold_tilde_tilde *x, t_symbol *s, int argc, t_atom *argv)
{
  t_float coarse, fine;

  if((argc > 0) && (IS_A_FLOAT(argv, 0)))
    coarse = atom_getfloatarg(0, argc, argv);
  else
    coarse = 0.0f;
  if((argc > 1) && (IS_A_FLOAT(argv, 1)))
    fine = atom_getfloatarg(1, argc, argv);
  else
    fine = 0.0f;
  x->x_lastout = iem_dp_calc_sum(coarse, fine);
}

void samphold_tilde_tilde_setup(void)
{
    samphold_tilde_tilde_class = class_new(gensym("samphold~~"),
        (t_newmethod)samphold_tilde_tilde_new, 0, sizeof(t_samphold_tilde_tilde), 0, 0);
    CLASS_MAINSIGNALIN(samphold_tilde_tilde_class, t_samphold_tilde_tilde, x_f);
    class_addmethod(samphold_tilde_tilde_class, (t_method)samphold_tilde_tilde_set,
        gensym("set"), A_GIMME, 0);
    class_addmethod(samphold_tilde_tilde_class, (t_method)samphold_tilde_tilde_reset,
        gensym("reset"), A_GIMME, 0);
    class_addmethod(samphold_tilde_tilde_class, (t_method)samphold_tilde_tilde_dsp,
        gensym("dsp"), A_CANT, 0);
}
