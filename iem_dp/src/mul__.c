/* For information on usage and redistribution, and for a DISCLAIMER OF ALL
* WARRANTIES, see the file, "LICENSE.txt," in this distribution.

iem_dp written by IOhannes m zmoelnig, Thomas Musil, Copyright (c) IEM KUG Graz Austria 1999 - 2007 */
/* double precision library */

#include "m_pd.h"
#include "iemlib.h"
#include "iem_dp.h"


/* ------------------------  mul__ or *__  ---------------------------- */
/* based on miller's *, which is part of pd, only with double precision */

static t_class *mul___class;

typedef struct _mul__
{
  t_object  x_obj;
  t_float   x_coarse_left;
  t_float   x_fine_left;
  t_float   x_coarse_right;
  t_float   x_fine_right;
  t_outlet  *x_out_coarse;
  t_outlet  *x_out_fine;
} t_mul__;

static void mul___bang(t_mul__ *x)
{
  double dprod;
  t_float fprod;

  dprod = iem_dp_calc_sum(x->x_coarse_left, x->x_fine_left) * iem_dp_calc_sum(x->x_coarse_right, x->x_fine_right);
  fprod = iem_dp_cast_to_float(dprod);
  outlet_float(x->x_out_fine, iem_dp_calc_residual(dprod, fprod));
  outlet_float(x->x_out_coarse, fprod);
}

static void mul___float(t_mul__ *x, t_floatarg f)
{
  x->x_coarse_left = f;
  mul___bang(x);
}

static void *mul___new(t_symbol *s, int ac, t_atom *av)
{
  t_mul__ *x = (t_mul__ *)pd_new(mul___class);

  floatinlet_new(&x->x_obj, &x->x_fine_left);
  floatinlet_new(&x->x_obj, &x->x_coarse_right);
  floatinlet_new(&x->x_obj, &x->x_fine_right);
  x->x_coarse_left = 0.0f;
  x->x_fine_left = 0.0f;
  if((ac > 0) && (IS_A_FLOAT(av, 0)))
    x->x_coarse_right = atom_getfloatarg(0, ac, av);
  else
    x->x_coarse_right = 0.0f;
  if((ac > 1) && (IS_A_FLOAT(av, 1)))
    x->x_fine_right = atom_getfloatarg(1, ac, av);
  else
    x->x_fine_right = 0.0f;
  x->x_out_coarse = outlet_new(&x->x_obj, &s_float);
  x->x_out_fine = outlet_new(&x->x_obj, &s_float);
  return (x);
}

static void mul___free(t_mul__ *x)
{
}

void mul___setup(void)
{
  mul___class = class_new(gensym("mul__"), (t_newmethod)mul___new, 0, sizeof(t_mul__), 0, A_GIMME, 0);
  class_addcreator((t_newmethod)mul___new, gensym("*__"), A_GIMME, 0);
  class_addcreator((t_newmethod)mul___new, gensym("*''"), A_GIMME, 0);
  class_addbang(mul___class, mul___bang);
  class_addfloat(mul___class, mul___float);
}
