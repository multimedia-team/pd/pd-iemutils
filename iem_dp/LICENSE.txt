iem_dp - dynamic library for pd; calculates pd signals or pd float messages in double precision.

The goal of this library is to allow more than 24 bit accurate access to arrays and delay-lines.
That means: with 32-bit IEEE floatingpoint we have a 23 bit mantissa with a resolution of 1 to 8388608.
If you want to access to an array ( size greater than 8388608 ) by tabread4, you get a staircase-shaped output.
The objects of this library work internal with double or 32-bit integer instead 24-bit mantissa of 32-bit float. To connect this higher resolution, we need 2 signal or 2 message cords. One for the float casted number and one for the difference to the higher resolution number.

Copyright (C) 2000-2013 Thomas MUSIL [musil_at_iem.at]

IEM - Institute of Electronic Music and Acoustics, Graz
Inffeldgasse 10/3, 8010 Graz, Austria
http://iem.at



    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details ( GnuGPL.txt ).
      (e.g.  http://www.gnu.org/copyleft/gpl.html)

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

         Graz, 28 June 2013
            Thomas Musil