triggerize
==========

  - replace fan-outs with [t a a ...]
  - when selecting a single line: insert [t a]
  - when selecting a [trigger]: insert "a" at the beginning

# Usage

### removing fan-outs
- Select objects with fan-outs
- Press <kbd>Control</kbd>+<kbd>t</kbd>
- the (msg) fan-outs will be replaced by (connected) [t a a] objects

### removing all fan-outs
- Select All objects (<kbd>Control</kbd>-<kbd>a</kbd>)
- Press <kbd>Control</kbd>+<kbd>t</kbd>
- all fan-outs will be replaced by (connected) [t a a] objects

### adding new left-hand outlet to [trigger]
- Select a [trigger] object
- Press <kbd>Control</kbd>+<kbd>t</kbd>
- a new (unconnected) left-most outlet of type "a" will be created.

### inserting triggers
- Select a single (msg) connection
- Press <kbd>Control</kbd>+<kbd>t</kbd>
- the connection will be replaced by a (connected) `[t a]` object

### inserting nop~ for signals
- Select a single signal connection
- Press <kbd>Control</kbd>+<kbd>t</kbd>
- the connection will be replaced by a (connected) `[pd nop~]` object

You can also use the menu:
- `Edit` -> `Triggerize`

OSX users should use <kbd>Cmd</kbd> instead of <kbd>Control</kbd>

# Installation

## Building
Build the external:

    make

If you have an unusual setup (and/or your Pd installation is at a place not
found by the compiler), you can parameterize the build. Try this first:

    make help


## Installing
Put both the externals (`triggerize.pd_linux` or similar) and the GUI plugin
(`triggerize-plugin.tcl`) into a directory `triggerize-plugin`, and put that
into a place where Pd can find it.

     make install

E.g.

    $ ls ~/.local/lib/pd/extra/triggerize-plugin/
    ~/.local/lib/pd/extra/triggerize-plugin/triggerize.pd_linux
    ~/.local/lib/pd/extra/triggerize-plugin/triggerize-plugin.tcl

# BUGS

## none known
TODO

# AUTHORS

IOhannes m zmölnig

# LICENSE
`triggerize` is released under the Gnu GPL version 2 (or later).
See LICENSE.md for the full text of the GPLv2.

## Special license grant to Miller Puckette
I hereby grant Miller S. Puckette the exclusive right to include
`triggerize` into Pure Data (Pd) under the BSD 3-clause license under
which Pd is currently released.

Once it has been included into Pd it is of course re-distributable under
that license. Until then, the Gnu GPL v2 (or later) applies.
